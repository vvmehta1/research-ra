package com.file;

import java.io.Serializable;

public class DefaultList implements Serializable {
	
	private String title;
	 private String [] subconcepts; 
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getSubconcepts() {
		return subconcepts;
	}

	public void setSubconcepts(String[] subconcepts) {
		this.subconcepts = subconcepts;
	}

	public DefaultList()
	{
		subconcepts[0] = "one";
		subconcepts[1] = "onfe";
		subconcepts[2] = "onggge";
	}
}
