package com.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.ws.RequestWrapper;

import com.file.SnowballStemmer;

/**
 * Servlet implementation class Compute
 */
@WebServlet("/Compute")
@MultipartConfig
public class Compute extends HttpServlet implements Serializable {
	int wordcount=0;
	int run = 0;
	Vector<String> stopwords = new Vector<String>();
	Vector<String> javawords = new Vector<String>();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpsServlet()
     */
    public Compute() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.println("Hell Unleashed...");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(run == 0 || stopwords.isEmpty())
		{
			stopwords = StopWordElimination.getwords();
			run++;
		}
		if(javawords.isEmpty())
		{
			javawords = StopWordElimination.JavaGlossary();
		}
		Part filepart = request.getPart("file");
		String filename = getFileName(filepart);
		System.out.println("File Transfered: "+filename);
		InputStream filecontent = filepart.getInputStream();
		BufferedReader read = new BufferedReader(new InputStreamReader(filecontent));
		StringBuffer input = new StringBuffer();
		String line = "";
		int character;
		//while((line = read.readLine()) != null)
			//System.out.println(line);
		Map<String,Integer> results = new HashMap<String,Integer>();
		//Map<String,Integer> trial = new HashMap<String,Integer>();
		
		// Remove words already present, remove them from the list being populated. 
		while ((character = read.read()) != -1) {
		    char ch = (char) character;
		    if (Character.isWhitespace((char) ch)) { // || ch == '\n'
			if (input.length() > 0) {
			    
			    System.out.println(input.toString());
			    String term = input.toString();
			    term = term.replaceAll("\\W", "");
			    term = term.replaceAll("\\s", "");
			    try{
			    	Class stemmmClass = Class.forName("com.file.englishStemmer");
			    	SnowballStemmer stemmerr = (SnowballStemmer) stemmmClass.newInstance();
			    	stemmerr.setCurrent(term);
			    	stemmerr.stem();
			    	term = stemmerr.getCurrent();
			    }
			    catch(Exception e)
			    {
			    	e.printStackTrace();
			    }
			    
			    
			    if(!term.equals("") && !term.equals(" ") && (term.length() > 1) && !stopwords.contains(term))
			    	{   
			    		javawords.remove(term);
			    		
			    		if(results.containsKey(term)) // If Key is Present
			    		{
			    			Integer count = ((Integer)results.get(term).intValue());
			    			count++;
			    			results.put(term,count);
			    			wordcount++;
			    			
			    		}
			    		else
			    		{
			    			results.put(term,1);
			    			wordcount++;
			    		}
			    }
			    
			    
			    input.delete(0, input.length());
			}
		    } else {
			input.append(Character.toLowerCase(ch));
		    }
		}
		
		if (input.length() > 0) {
		    
		    System.out.println(input.toString());
		    String term = input.toString();
		    term = term.replaceAll("\\W", "");
		    term = term.replaceAll("\\s", "");
		    
		    if(!term.equals("") && !term.equals(" ") && (term.length()>1))
		    {
		    	javawords.remove(term);
		    	if(results.containsKey(term)) // If Key is Present
		    	{
		    		Integer count = ((Integer)results.get(term).intValue());
		    		count++;
		    		results.put(term,count);
		    		wordcount++;
		    	}	
		    else
				{
					results.put(term,1);
					wordcount++;
				}
		    }
		    
		    
		    input.delete(0, input.length());
		}
		MapUtil a = new MapUtil();
		results = a.sortByValue(results);
		request.setAttribute("concepts", wordcount);
		Vector luck = calculateWeight(results, wordcount);
		request.setAttribute("finalresult", luck);
		wordcount = 0;
		request.setAttribute("defaultwords", javawords);
		request.setAttribute("results", results);
		//request.setAttribute("priority", view);
		
		Vector<String> concepts = StopWordElimination.GetMainContents();
		HashMap subconcepts = StopWordElimination.GetSecContents();
		request.setAttribute("subconceptslist", subconcepts);
		request.setAttribute("mainconceptslist", concepts);
		request.getRequestDispatcher("Formatting.jsp").forward(request, response);
		
	}
	private static String getFileName(Part part) {
	    for (String cd : part.getHeader("content-disposition").split(";")) {
	        if (cd.trim().startsWith("filename")) {
	            String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
	            return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
	        }
	    }
	    return null;
	}
	
	private Vector calculateWeight(Map<String,Integer> calculate, int sum)
	{
		double[] weights = new double[calculate.size()];
		int i = 0;
		Vector send = new Vector();
		for (Map.Entry<String, Integer> entry : calculate.entrySet())
		{
			Results a = new Results();
			a.count = entry.getValue();
			weights[i] = (a.count/sum) * 100;
			a.word = entry.getKey();
			a.priority = weights[i];
			send.add(a);
			i++;
		   
		}
		
		
		return send;
	}
	

}

class Results
{
	String word;
	int count;
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public double getPriority() {
		return priority;
	}
	public void setPriority(double priority) {
		this.priority = priority;
	}
	double priority;
}
class MapUtil
{
    public <K, V extends Comparable<? super V>> Map<K, V> 
        sortByValue( Map<K, V> map )
    {
        List<Map.Entry<K, V>> list =
            new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }
}
