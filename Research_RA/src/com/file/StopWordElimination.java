package com.file;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;

public class StopWordElimination implements Serializable {
	
	
	
	static Vector getwords()
	{
		Vector<String> stopwords = new Vector<String>();
		stopwords.add("the");
		stopwords.add("an");
		stopwords.add("has");
		stopwords.add("you");
		stopwords.add("to");
		stopwords.add("of");
		stopwords.add("is");
		stopwords.add("be");
		stopwords.add("can");
		stopwords.add("it");
		stopwords.add("are");
		stopwords.add("and");
		stopwords.add("than");
		stopwords.add("will");
		stopwords.add("this");
		stopwords.add("from");
		stopwords.add("if");
		stopwords.add("in");
		stopwords.add("an");
		return stopwords;
	}
	
	static Vector JavaGlossary()
	{
		Vector<String> javawords = new Vector<String>();
		javawords.add("abstract");
		javawords.add("class");
		javawords.add("method");
		javawords.add("awt");
		javawords.add("access");
		javawords.add("control");
		javawords.add("acid");
		javawords.add("parameter");
		javawords.add("list");
		javawords.add("api");
		javawords.add("applet");
		javawords.add("argument");
		javawords.add("array");
		javawords.add("ascii");
		javawords.add("atomic");
		javawords.add("authentication");
		javawords.add("authorization");
		javawords.add("autoboxing");
		javawords.add("bean");
		javawords.add("binary");
		javawords.add("operator");
		javawords.add("bit");
		javawords.add("bitwise");
		javawords.add("block");
		javawords.add("bool");
		javawords.add("boolean");
		javawords.add("break");
		javawords.add("byte");
		javawords.add("bytecode");
		javawords.add("case");
		javawords.add("casting");
		javawords.add("catch");
		javawords.add("char");
		javawords.add("class");
		javawords.add("method");
		javawords.add("variable");
		javawords.add("classpath");
		javawords.add("client");
		javawords.add("codebase");
		javawords.add("comment");
		javawords.add("commit");
		javawords.add("compilation");
		javawords.add("compiler");
		javawords.add("unit");
		javawords.add("compositing");
		javawords.add("constructor");
		javawords.add("const");
		javawords.add("continue");
		javawords.add("conversational");
		javawords.add("state");
		javawords.add("corba");
		javawords.add("core");
		javawords.add("class");
		javawords.add("packages");
		javawords.add("credentials");
		javawords.add("critical");
		javawords.add("section");
		javawords.add("declaration");
		javawords.add("default");
		javawords.add("definition");
		javawords.add("delegation");
		javawords.add("deprecation");
		javawords.add("derived");
		javawords.add("distributed");
		javawords.add("application");
		javawords.add("do");
		javawords.add("dom");
		javawords.add("double");
		javawords.add("precision");
		javawords.add("dtd");
		javawords.add("else");
		javawords.add("embedded");
		javawords.add("java");
		javawords.add("technology");
		javawords.add("encapsulation");
		javawords.add("enum");
		javawords.add("enumerated");
		javawords.add("type");
		javawords.add("exception");
		javawords.add("handler");
		javawords.add("content");
		javawords.add("executable");
		javawords.add("extends");
		javawords.add("final");
		javawords.add("finally");
		javawords.add("float");
		javawords.add("for");
		javawords.add("ftp");
		javawords.add("parameter");
		javawords.add("list");
		javawords.add("garbage");
		javawords.add("collection");
		javawords.add("generic");
		javawords.add("goto");
		javawords.add("group");
		javawords.add("gui");
		javawords.add("hexadecimal");
		javawords.add("hierarchy");
		javawords.add("html");
		javawords.add("http");
		javawords.add("https");
		javawords.add("idl");
		javawords.add("identifier");
		javawords.add("iiop");
		javawords.add("impersonation");
		javawords.add("if");
		javawords.add("implements");
		javawords.add("import");
		javawords.add("inheritance");
		javawords.add("instance");
		javawords.add("instance");
		javawords.add("hierarchy");
		javawords.add("method");
		javawords.add("variable");
		javawords.add("instanceof");
		javawords.add("int");
		javawords.add("interface");
		javawords.add("internet");
		javawords.add("interpreter");
		javawords.add("ip");
		javawords.add("jain");
		javawords.add("jar");
		javawords.add("interface");
		javawords.add("rmi");
		javawords.add("jvm");
		javawords.add("virtual");
		javawords.add("machine");
		javawords.add("javabean");
		javawords.add("javascript");
		javawords.add("jdbc");
		javawords.add("jdk");
		javawords.add("jfc");
		javawords.add("jndi");
		javawords.add("keyword");
		javawords.add("lexical");
		javawords.add("linker");
		javawords.add("literal");
		javawords.add("variable");
		javawords.add("local");
		javawords.add("long");
		javawords.add("member");
		javawords.add("method");
		javawords.add("multithreaded");
		javawords.add("native");
		javawords.add("new");
		javawords.add("null");
		javawords.add("object");
		javawords.add("design");
		javawords.add("oriented");
		javawords.add("octal");
		javawords.add("packages");
		javawords.add("orb");
		javawords.add("principal");
		javawords.add("ots");
		javawords.add("overloading");
		javawords.add("overriding");
		javawords.add("package");
		javawords.add("peer");
		javawords.add("persistence");
		javawords.add("pixel");
		javawords.add("key");
		javawords.add("primary");
		javawords.add("primitive");
		javawords.add("poa");
		javawords.add("principal");
		javawords.add("private");
		javawords.add("privilege");
		javawords.add("process");
		javawords.add("property");
		javawords.add("profiles");
		javawords.add("protected");
		javawords.add("public");
		javawords.add("raster");
		javawords.add("realm");
		javawords.add("reference");
		javawords.add("return");
		javawords.add("rollback");
		javawords.add("root");
		javawords.add("rmi");
		javawords.add("rpc");
		javawords.add("runtime");
		javawords.add("system");
		javawords.add("sandbox");
		javawords.add("sax");
		javawords.add("scope");
		javawords.add("ssl");
		javawords.add("attributes");
		javawords.add("security");
		javawords.add("context");
		javawords.add("domain");
		javawords.add("policy");
		javawords.add("serialization");
		javawords.add("security");
		javawords.add("serialization");
		javawords.add("short");
		javawords.add("precision");
		javawords.add("single");
		javawords.add("soap");
		javawords.add("sql");
		javawords.add("static");
		javawords.add("field");
		javawords.add("method");
		javawords.add("stream");
		javawords.add("subarray");
		javawords.add("subclass");
		javawords.add("subtype");
		javawords.add("superclass");
		javawords.add("super");
		javawords.add("supertype");
		javawords.add("switch");
		javawords.add("swing");
		javawords.add("synchronized");
		javawords.add("tcp");
		javawords.add("ip");
		javawords.add("tcpip");
		javawords.add("client");
		javawords.add("thin");
		javawords.add("thread");
		javawords.add("this");
		javawords.add("throws");
		javawords.add("throw");
		javawords.add("transaction");
		javawords.add("isolation");
		javawords.add("level");
		javawords.add("manager");
		javawords.add("transient");
		javawords.add("try");
		javawords.add("type");
		javawords.add("unicode");
		javawords.add("uri");
		javawords.add("url");
		javawords.add("urn");
		javawords.add("variable");
		javawords.add("machine");
		javawords.add("virtual");
		javawords.add("void");
		javawords.add("volatile");
		javawords.add("server");
		javawords.add("web");
		javawords.add("webserver");
		javawords.add("while");
		javawords.add("readable");
		javawords.add("files");
		javawords.add("world");
		javawords.add("wrapper");
		javawords.add("www");
		javawords.add("xml");
		
		Vector<String> finalwords = new Vector<String>();
		// Do the stemming here
		for(String get : javawords)
		{
			String stemedword;
			try{
			    	Class stemmmClass = Class.forName("com.file.englishStemmer");
			    	SnowballStemmer stemmerr = (SnowballStemmer) stemmmClass.newInstance();
			    	stemmerr.setCurrent(get);
			    	stemmerr.stem();
			    	stemedword = stemmerr.getCurrent();
			    	finalwords.add(stemedword);
			    }
			    catch(Exception e)
			    {
			    	e.printStackTrace();
			    }
		}
		javawords.clear();
		return finalwords;
	}
	
	public static Vector GetMainContents(){
		Vector<String> concepts = new Vector<String>();
		ArrayList<String> sendclient = new ArrayList<String>();
		concepts.add("Variables");
		concepts.add("Primitive Data Types");
		concepts.add("Constants");
		concepts.add("Arithmetic Operations");
		concepts.add("Strings");
		concepts.add("Boolean Expressions");
		concepts.add("If Else");
		concepts.add("Switch");
		concepts.add("Loops");
		concepts.add("Loops For");
		concepts.add("Loops While");
		concepts.add("Loops Do While");
		concepts.add("Loops Nested Loops");
		concepts.add("Objects");
		concepts.add("Classes");
		concepts.add("Arrays");
		concepts.add("Two Dimensional Arrays");
		concepts.add("Arraylist");
		concepts.add("Inheritance");
		concepts.add("Interface");
		
		//String jsonvalue = null;
		//ObjectMapper mapper = new ObjectMapper();
		//mapper.setVisibility(JsonMethod.FIELD, Visibility.ANY);
		
		
		/*for(String concept: concepts)
		{
			DefaultList temp = new DefaultList();
			// Put it later
			temp.setTitle(concept);
			
			try{
				jsonvalue = mapper.writeValueAsString(temp);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			if(jsonvalue != null)
				sendclient.add(jsonvalue);
			
			
		}*/
		return concepts;
	}
	
	static HashMap GetSecContents(){
		HashMap<String, String> concepts = new HashMap<String,String>();
		concepts.put("Variables","One Two Three");
		concepts.put("Primitive Data Types","Four Five");
		concepts.put("Constants","Seven Ten");
		concepts.put("Arithmetic Operations","Ninety Hundred ThirtyOne");
		concepts.put("Strings","Olla Amigo What re you donig");
		concepts.put("Boolean Expressions","HelloWorld Do This Now");
		concepts.put("If Else","Take this you hola");
		concepts.put("Switch","cannot think of more");
		concepts.put("Loops","we can do this");
		concepts.put("Loops For","will add concepts later on");
		concepts.put("Loops While","let me handle this baby");
		concepts.put("Loops Do While","aishrayra jill jack");
		concepts.put("Loops Nested Loops","love is blind");
		concepts.put("Objects","told you be careful");
		concepts.put("Classes","why you no serious");
		concepts.put("Arrays","finish thesis come on");
		concepts.put("Two Dimensional Arrays","quit my cashier job");
		concepts.put("Arraylist","hello this is confusing");
		concepts.put("Inheritance","meet professor take guidance");
		concepts.put("Interface","completed this now never");
		
		return concepts;
	}
	
	

}
