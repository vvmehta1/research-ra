<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
	#charts {margin-left:30px; margin-right: auto; display:block;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Demo</title>
   <script type="text/javascript" src="http://mbostock.github.com/d3/d3.js?2.5.0"></script>
   <script type="text/javascript" src="http://mbostock.github.com/d3/d3.layout.js?2.5.0"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script type="text/javascript" src="./scrolltable.js"></script>

<link href="./bootstrap.min.css" rel="stylesheet"/>
<link href="./bootstrap-theme.css" rel="stylesheet"/>
<link href="./bootstrap.css" rel="stylesheet"/>
<link href="./dashboard.css" rel="stylesheet">
    <script>
function myFunction() {
    var table = document.getElementById("myTable");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var element1 = document.createElement("input");
    element1.type="text";
    cell1.appendChild(element1);
    var element2 = document.createElement("input");
    element2.type="text";
    cell2.innerHTML = "#";
    cell3.appendChild(element2);
    
}
function calculate()
{
	var table = document.getElementById("myTable");
	var value = ${concepts};
	for (var i = 0, row; row = table.rows[i]; i++) {
	   //iterate through rows
	   //rows would be accessed using the "row" variable assigned in the for loop
	   for (var j = 0, col; col = row.cells[j]; j++) {
	     //iterate through columns
	     //columns would be accessed using the "col" variable assigned in the for loop
	     if(j==1)
	    	 {
	    	 	
	    	 	row.cells[2].value = (col.innerHTML/value)*100;
	    	 	alert(row.cells[2].value);
	    	 }
	   }  
	}
	
}

function addrow(capture)
{
	var textt = capture.value;
	var table = document.getElementById("myTable");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var element1 = document.createElement("input");
    element1.type="text";
    element1.value = textt;
    cell1.appendChild(element1);
    var element2 = document.createElement("input");
    element2.type="text";
    cell2.innerHTML = "#";
    cell3.appendChild(element2);
	
}
</script>

</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>     
        <!-- col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main -->
        <div class="container">
          <h1 class="page-header">Keywords Setup </h1>
          
           <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              
              <h4>Key Words</h4>
              
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <h4><input class="form-control" id="disabledInput" type="text" placeholder='${concepts}' disabled></h4>
              
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <h4><button type="button" class="btn btn-primary btn-lg btn-block" onclick="myFunction()">Add Concept</button></h4>
              
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <h4><button type="button" class="btn btn-primary btn-lg btn-block" onclick="calculate()">Compute</button></h4>
              
            </div>
          </div>
     
			<div class="row placeholders">
			<div class="col-md-5 placeholder">
			
			
		<div class="table-responsive">
        <table class="table table-striped" border="1" id="myTable1" height="200px"> <!--  border="1" width="75%" -->
        <thead>
        <tr>
        	 <!--  <th>Selector</th>-->
            <th>Word</th>
            <th>Add</th>
        </tr>
      	</thead>
      	<tbody>
        <c:forEach var="entry" items="${defaultwords}">
            
            <tr>
            
            
            	<!-- <td><div class="checkbox" ><input type="checkbox" value=""></div></td> -->
                <td><input id="here" type="text" value='<c:out value="${entry}"/>'></td>
                <td><button value='<c:out value="${entry}"/>' onclick="addrow(this)"> Add</button></td>
                
            </tr>
            
        </c:forEach>
        </tbody>
    	</table>
        </div>
			
			</div>
			<div class="col-md-7 placeholder">
			
		<div class="scrollwrap">
		<div class="table-responsive">
        <table class="table table-hover" border="1" id="myTable" height="200px"> <!--  border="1" width="75%" -->
        <thead>
            
        <tr>
        	
            <th>Word</th>
            <th>Count</th>
            <th>Importance</th>
        </tr>
      	</thead>
      	<tbody>
        <c:forEach var="entry" items="${results}">
            <tr>
            	
                <td><input id="here" type="text" value='<c:out value="${entry.key}"/>'></td>
                <td><input type="text" value='<c:out value="${entry.value}"/>'></td>
                
                <td><input type="text"></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </div>
    </div>
    
    
    </div>
	</div>
	
	
	<div class="row placeholders">
	<div class="col-md-5 placeholder">
	<script type="text/javascript">
	$(function() {
        $('#myTable').scrollTableBody();
    });
	$(function() {
        $('#myTable1').scrollTableBody();
    });
	
    d3.selection.prototype.moveToFront = function() {
      return this.each(function(){
        this.parentNode.appendChild(this);
      });
    };
    var drag = d3.behavior.drag()
        .on("drag", function(d,i) {
          d3.select(this).moveToFront();
          if(d.x + d3.event.dx >= 0 && d.x + d3.event.dx <= 700 &&
             d.y + d3.event.dy >= 0 && d.y + d3.event.dy <= 500){
            d.x += d3.event.dx
            d.y += d3.event.dy
            d3.select(this).attr("transform", function(d,i){
                return "translate(" + [ d.x,d.y ] + ")"
            })
          }
        })
        .on("dragend", function(d,i){
          
        });


    function face_factory(classname, data, x, y, r)
    { 
        //data format
        //{ "x": 0 - 1 , "y": 0 -1, "z": 0-1 }
        //color could be made a parameter
        //var arc = d3.svg.arc().outerRadius(r)
        //var donut = d3.layout.pie();

        var face = d3.select("#charts")
            .append("svg:g")
                //.data([data.sort(d3.descending)])
                //.data([data])
                .data([ {"x":x, "y":y} ])
                .attr("class", classname)
                .attr("transform", "translate(" + x + "," + y + ")")
                .call(drag)
                .on("click", function (){d3.select(this).moveToFront();})
                /*.on("mouseover", function (){
                    d3.select(this)
                      .selectAll("circle")
                      .attr("r", r+5);
                 })
                 .on("mouseout", function (){
                    d3.select(this)
                      .selectAll("circle")
                      .attr("r", r);
                 })*/;


        console.log("make head");
        var head_color = d3.scale.linear()
            .domain([0, 1])
            .interpolate(d3.interpolateRgb)
            .range(["#ff0000", "#0000ff"]);

        var head = face.append("svg:circle")
                .attr("class", "head")
                .attr("fill", function(d){ return head_color(data.x); })
                .attr("fill-opacity", .8)
                .attr("stroke", "#000")
                .attr("stroke-width", 4)
                .attr("r", r);

        console.log("make mouth");
        var mouth_x = [0, .5, 1];

        var mouth_x_range = d3.scale.linear()
            .domain([0, 1])
            .range([-r/2, r/2]);


        var mouth_y_range = d3.scale.linear()
            .domain([0, 1])
            .range([-r/2, r/2]);

        var mouth_y = [.5, data.y, .5];
        console.log(mouth_y)

        var mouth_line = d3.svg.line()
            .x(function(d,i) {
                return mouth_x_range(mouth_x[i]);
            })
            .y(function(d,i) {
                return mouth_y_range(mouth_y[i]);
            })
            .interpolate("basis");

        /*
        var mouth = face.append("svg:path")
                .attr("class", "mouth")
                .attr("stroke", "#000")
                .attr("stroke-width", 4)
                .attr("fill", "none")
                .attr("transform", "translate(" + [0, r/3] + ")")
                .attr("d", mouth_line(mouth_x));
                */

        /*console.log("create eyes")
        var eyer = r/10 + data.z * (r/3);
        console.log(eyer);
        var left_eye = face.append("svg:circle")
            .attr("class", "eyes")
            .attr("stroke", "#000")
            .attr("fill", "none")
            .attr("stroke-width", 4)
            .attr("transform", "translate(" + [-r/2.5, -r/3] + ")")
            .attr("r", eyer);

        //eyer = r/10 + data.w * (r/3);
        var right_eye = face.append("svg:circle")
            .attr("class", "eyes")
            .attr("stroke", "#000")
            .attr("fill", "none")
            .attr("stroke-width", 4)
            .attr("transform", "translate(" + [r/2.5, -r/3] + ")")
            .attr("r", eyer);
*/
        var text = face.append("svg:text")
            .text("Abcde")
            .attr("y", ".5em")
            .attr("transform", "translate(" + [0, r/12] + ")")
            .attr("text-anchor", "middle")
            .attr("font-weight", 500)
            .attr("font-size", 14)
            .attr("font-family", "Helvetica")
            .attr("fill", "#ff0")
            .attr("stroke", "none")
            .attr("pointer-events", "none") 

    }

    var w = 700;
    var h = 500;

    //setup svg canvas
    d3.select("body")
        .append("svg:svg")
            .attr("width", w)
            .attr("height", h)
            .attr("id", "charts")
            //.on("click", clickypie)
            .append("svg:rect")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("stroke", "#000")
                
                .attr("stroke-width", 2)
                .attr("fill", "none")

    
    
    //r = 100;
    for(i = 0; i < 10; i++)
    {
        var r = 25;
        var data = { "x":Math.random(), "y":Math.random(), "z":Math.random(), "w":Math.random() };
        x = Math.random() * w
        y = Math.random() * h
        face_factory("face"+i, data, x, y, r);
    }
    
   
   </script>
          
	</div>
	</div>	
         
         
        </div>
    <div>
    <p align="center"> <strong style="font-size: 150%;"> Cluster Visualization of generated keywords </strong> </p>
    </div>
    
</body>
</html>