
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<link href="./bootstrap.min.css" rel="stylesheet"/>
<link href="./bootstrap-theme.css" rel="stylesheet"/>
<link href="./bootstrap.css" rel="stylesheet"/>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Results</title>

</head>
<body>
<div>
</div>
<div class="container">
<div class="form-group">
      <label for="inputPassword" class="col-sm-2 control-label">Key Words: </label>
      <div class="col-sm-2">
        <input class="form-control" id="disabledInput" type="text" placeholder='${concepts}' disabled>
      </div>
      <div class="col-sm-4">
      <button type="button" class="btn btn-primary btn-lg btn-block" onclick="myFunction()">Add Concept</button>
      </div>
      <div class="col-sm-4">
      <button type="button" class="btn btn-primary btn-lg btn-block" onclick="calculate()">Compute</button>
      </div>
    </div>

<table class="table table-striped" width="75%" id="myTable"> <!--  border="1" width="75%" -->
        <tr>
        	
            <th>Word</th>
            <th>Count</th>
            <th>Importance</th>
        </tr>
      
        <c:forEach var="entry" items="${results}">
            <tr>
            	
                <td><input id="here" type="text" value='<c:out value="${entry.key}"/>'></td>
                <td><input type="text" value='<c:out value="${entry.value}"/>'></td>
                
                <td><input type="text"></td>
            </tr>
        </c:forEach>
    </table>
    </div>
    
    
    
    <script>
function myFunction() {
    var table = document.getElementById("myTable");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var element1 = document.createElement("input");
    element1.type="text";
    cell1.appendChild(element1);
    var element2 = document.createElement("input");
    element2.type="text";
    cell2.innerHTML = "#";
    cell3.appendChild(element2);
    
}
function calculate()
{
	var table = document.getElementById("myTable");
	var value = ${concepts};
	for (var i = 0, row; row = table.rows[i]; i++) {
	   //iterate through rows
	   //rows would be accessed using the "row" variable assigned in the for loop
	   for (var j = 0, col; col = row.cells[j]; j++) {
	     //iterate through columns
	     //columns would be accessed using the "col" variable assigned in the for loop
	     if(j==1)
	    	 {
	    	 	
	    	 	row.cells[2].value = (col.innerHTML/value)*100;
	    	 	alert(row.cells[2].value);
	    	 }
	   }  
	}
	
}
</script>

<p id="demo"> </p>

<p id="demo1"> </p>



</body>
</html>